package com.example.cashbackms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.time.LocalDateTime;

import com.example.cashbackms.enums.ConstantEnum;
import com.example.cashbackms.enums.LangEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "constant_info")
@Data
@NoArgsConstructor
public class ConstantInfo {

    @Id
    @JoinColumn(name = "id")
    private Long id;

    @Column(name = "description")
    @Enumerated(EnumType.STRING)
    private ConstantEnum type;

    @Column(name = "value")
    private String value;

    @Column(name = "lang")
    @Enumerated(EnumType.STRING)
    private LangEnum lang;

    @Column(name = "created_date")
    @CreationTimestamp
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdDate;
}
