package com.example.cashbackms.entity;

public interface MerchantDetailView {

    MerchantDetail getMerchantEntity();

    String info();
}
