package com.example.cashbackms.controller;

import com.example.cashbackms.dto.response.CommonResponse;
import com.example.cashbackms.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping
    public ResponseEntity<CommonResponse<?>> getAllCategories(@RequestHeader String lang) {
        return CommonResponse.success("success", categoryService.getAll(lang));
    }
}