package com.example.cashbackms.controller;

import com.example.cashbackms.dto.response.CommonResponse;
import com.example.cashbackms.service.MerchantService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/merchants")
@RequiredArgsConstructor
public class MerchantController {
    private final MerchantService merchantService;

    @GetMapping
    public ResponseEntity<CommonResponse<?>> getMerchantList(
            @RequestHeader String lang,
            @RequestParam(required = false) Long categoryId,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10_000") Integer size) {
        return CommonResponse.success("success", merchantService.getMerchantList(lang, categoryId, page, size));
    }

    @GetMapping("/{id}/details")
    public ResponseEntity<CommonResponse<?>> getMerchantDetailsById(
            @RequestHeader String lang,
            @PathVariable Long id) {
        return CommonResponse.success("success", merchantService.getMerchantDetailsById(id, lang));
    }
}
