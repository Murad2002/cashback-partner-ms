package com.example.cashbackms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CashbackMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CashbackMsApplication.class, args);
    }

}
