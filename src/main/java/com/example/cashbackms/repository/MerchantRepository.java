package com.example.cashbackms.repository;

import java.util.List;

import com.example.cashbackms.entity.Merchant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchantRepository extends PagingAndSortingRepository<Merchant, Long> {

    @Query("select m from Merchant m " +
            "where m.category.id = :categoryId ")
    Page<Merchant> findAllByCategory(Long categoryId, Pageable pageable);

}
