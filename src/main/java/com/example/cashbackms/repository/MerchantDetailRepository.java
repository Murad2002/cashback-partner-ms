package com.example.cashbackms.repository;

import com.example.cashbackms.entity.MerchantDetail;
import com.example.cashbackms.entity.MerchantDetailView;
import com.example.cashbackms.enums.ConstantEnum;
import com.example.cashbackms.enums.LangEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchantDetailRepository extends PagingAndSortingRepository<MerchantDetail, Long> {

    @Query(value = "SELECT md.*, c.value " +
            "FROM merchant_details md, constant_info c  " +
            "WHERE md.merchant_id = ?1 AND c.description = ?2 AND c.lang = ?3", nativeQuery = true)
    MerchantDetailView findAllByCategory(Long merchantId, ConstantEnum description , LangEnum lang);

}
