package com.example.cashbackms.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CategoryRespDTO implements Serializable {
    private Long id;
    private String name;
    private String iconUrl;
}
