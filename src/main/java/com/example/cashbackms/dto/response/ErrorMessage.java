package com.example.cashbackms.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorMessage implements Serializable {

    private int code;
    private String errorMessage;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object invalidValue;
}