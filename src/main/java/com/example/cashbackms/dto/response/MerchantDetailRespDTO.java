package com.example.cashbackms.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MerchantDetailRespDTO implements Serializable {

    private Long id;
    private String info;
    private String taxits;
    private String instagramUrl;
    private String phone;
    private String webPageUrl;
    private List<String> addresses;

}
