package com.example.cashbackms.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("timestamp")
public class CommonResponse<T> {

    private int code;
    private String message;
    private T data;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ErrorMessage> errors;
    @JsonFormat(pattern = "ddMMyyyyHHmmss")
    private LocalDateTime timestamp;

    public static ResponseEntity<CommonResponse<?>> success(String message, Object data) {
        CommonResponse<Object> commonResponse = new CommonResponse<>();
        commonResponse.setCode(HttpStatus.OK.value());
        commonResponse.setMessage(message);
        commonResponse.setData(data);
        commonResponse.setTimestamp(LocalDateTime.now());
        return ResponseEntity.ok(commonResponse);
    }

    public static ResponseEntity<CommonResponse<Object>> error(String message, List<ErrorMessage> errors, HttpStatus status) {
        CommonResponse<Object> commonResponse = new CommonResponse<>();
        commonResponse.setCode(1);
        commonResponse.setMessage(message);
        commonResponse.setData(null);
        commonResponse.setErrors(errors);
        commonResponse.setTimestamp(LocalDateTime.now());
        return ResponseEntity.status(status).body(commonResponse);
    }

    public static ResponseEntity<CommonResponse<Object>> error(String message, HttpStatus status) {
        CommonResponse<Object> commonResponse = new CommonResponse<>();
        commonResponse.setCode(1);
        commonResponse.setMessage(message);
        commonResponse.setData(null);
        commonResponse.setTimestamp(LocalDateTime.now());
        return ResponseEntity.status(status).body(commonResponse);
    }
}
