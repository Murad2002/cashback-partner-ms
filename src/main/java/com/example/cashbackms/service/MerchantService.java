package com.example.cashbackms.service;

import java.util.List;

import com.example.cashbackms.dto.response.MerchantDetailRespDTO;
import com.example.cashbackms.dto.response.MerchantRespDTO;

public interface MerchantService {
    List<MerchantRespDTO> getMerchantList(String lang, Long categoryId, Integer page, Integer size);

    MerchantDetailRespDTO getMerchantDetailsById(Long id, String lang);
}
