package com.example.cashbackms.service;

import com.example.cashbackms.dto.response.CategoryRespDTO;

import java.util.List;

public interface CategoryService {

    List<CategoryRespDTO> getAll(String lang);
}
