package com.example.cashbackms.service.impl;

import com.example.cashbackms.dto.response.CategoryRespDTO;
import com.example.cashbackms.entity.Category;
import com.example.cashbackms.enums.LangEnum;
import com.example.cashbackms.repository.CategoryRepository;
import com.example.cashbackms.service.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Override
    public List<CategoryRespDTO> getAll(String lang) {
        return categoryRepository.findAll()
                .stream()
                .map(category -> entityToDto(category, LangEnum.valueOf(lang.toUpperCase())))
                .collect(Collectors.toList());
    }


    private CategoryRespDTO entityToDto(Category category, LangEnum langEnum) {
        return CategoryRespDTO.builder()
                .id(category.getId())
                .iconUrl(category.getIconUrl())
                .name(category.getCategoryName(langEnum))
                .build();
    }
}
