package com.example.cashbackms.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.example.cashbackms.dto.response.MerchantDetailRespDTO;
import com.example.cashbackms.dto.response.MerchantRespDTO;
import com.example.cashbackms.entity.Merchant;
import com.example.cashbackms.entity.MerchantDetailView;
import com.example.cashbackms.enums.ConstantEnum;
import com.example.cashbackms.enums.LangEnum;
import com.example.cashbackms.repository.MerchantDetailRepository;
import com.example.cashbackms.repository.MerchantRepository;
import com.example.cashbackms.service.MerchantService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class MerchantServiceImpl implements MerchantService {
    private final MerchantRepository merchantRepository;
    private final MerchantDetailRepository merchantDetailRepository;

    @Override
    public List<MerchantRespDTO> getMerchantList(String lang, Long categoryId, Integer page, Integer size) {
        Pageable sortedByNameAsc =
                PageRequest.of(page, size, Sort.by("name").ascending());

        if (categoryId == null)
            return entityToDto(merchantRepository.findAll(sortedByNameAsc).get(), lang);

        return entityToDto(merchantRepository.findAllByCategory(categoryId, sortedByNameAsc).get(), lang);

    }

    @Override
    public MerchantDetailRespDTO getMerchantDetailsById(Long id, String lang) {

        MerchantDetailView view = merchantDetailRepository.findAllByCategory(id,
                ConstantEnum.MERCHANT_INFO,
                LangEnum.valueOf(lang.toUpperCase()));

        return MerchantDetailRespDTO.builder()
                .id(view.getMerchantEntity().getId())
                .build();
    }

    private List<MerchantRespDTO> entityToDto(Stream<Merchant> merchantStream, String lang) {
        return merchantStream
                .map(merchant -> MerchantRespDTO.builder()
                        .id(merchant.getId())
                        .name(merchant.getName())
                        .iconUrl(merchant.getIconUrl())
                        .categoryName(merchant.getCategory().getCategoryName(LangEnum.valueOf(lang.toUpperCase())))
                        .cashbackPercent(merchant.getCashbackPercent())
                        .build())
                .collect(Collectors.toList());
    }
}
