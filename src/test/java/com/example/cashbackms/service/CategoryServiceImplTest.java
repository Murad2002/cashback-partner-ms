package com.example.cashbackms.service;

import com.example.cashbackms.dto.response.CategoryRespDTO;
import com.example.cashbackms.entity.Category;
import com.example.cashbackms.enums.LangEnum;
import com.example.cashbackms.repository.CategoryRepository;
import com.example.cashbackms.service.impl.CategoryServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTest {


    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private CategoryServiceImpl categoryService;

    @Test
    public void getAll_ShouldReturnListOfCategoryRespDTO() {
        // given
        Category category1 = new Category();
        category1.setId(1L);

        Category category2 = new Category();
        category2.setId(2L);

        List<Category> categoryList = Arrays.asList(category1, category2);

        LangEnum langEnum = LangEnum.EN;

        // when
        when(categoryRepository.findAll()).thenReturn(categoryList);

        // then
        List<CategoryRespDTO> resultDTOList = categoryService.getAll(langEnum.name());

        assertEquals(categoryList.size(), resultDTOList.size());
        assertEquals(category1.getId(), resultDTOList.get(0).getId());
        assertEquals(category1.getCategoryName(langEnum), resultDTOList.get(0).getName());
        assertEquals(category2.getId(), resultDTOList.get(1).getId());
        assertEquals(category2.getCategoryName(langEnum), resultDTOList.get(1).getName());

        verify(categoryRepository, times(1)).findAll();
    }
}